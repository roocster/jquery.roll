/* jquery.roll-1.1.js
 * jQuery.Roll plugin.
 * Author:      I. Navitsky.
 * Date:        23 nov 2013
 * Last updatd: 25 nov 2013 
 */
(function($) {
    var defaults = {
        'speed' : '500'
    };

    var methods = {
        init : function(options) {
			var handler  = this;
            var settings = $.extend({}, defaults, options);
            var data     = {
                'settings' : settings,
                'list'     : handler.find('ul'),
                'enable'   : true
            };

            var width = 0;
            data.list.find('li').each(function() {
                width += handler.outerWidth(true);
            });
            data.list.width(width);

            handler.data('roll', data);

            return handler;
        },
		stop: function() {
			var handler = this;
			var data    = handler.data('roll');
			if(!data.play) {
				return handler;
			}
			clearInterval(data.interval);
			data.play = false;
			
			handler.data('roll', data);
			
			return handler;
		},
		play: function(direction, delay) {
			var handler = this;
			var data    = handler.data('roll');
			if(data.play) {
				return handler;
			}
			data.play = true;
			var args  = arguments;
			
			data.interval = setInterval(function() {
				methods.slide.apply(handler, [args[0]])
			}, data.settings.speed + delay);
			
			handler.data('roll', data);
			
			return handler;
		},
        slide : function(direction) {
			var handler = this;
            var data    = handler.data('roll');
			
            function moveChild(position) {
                var el = data.list.find('li:' + position + '-child').remove();
                if(position == 'first') {
                    data.list.append(el);
                }
                else {
                    data.list.prepend(el);
                }
            }

            if(!data.enable) {
                return handler;
            }
            data.enable = false;
            handler.data('roll', data);

            var offset;
            switch(direction){
                case 'left': {
                    offset = data.list.find('li:first-child').outerWidth(true);
                    data.list.animate({'left': -offset}, data.settings.speed, function() {
                        moveChild('first');
                        data.list.css('left', 0);
                        data.enable = true;
                        handler.data('roll', data);
                    });
                    break;
                }
                case 'right': {
                    offset = data.list.find('li:last-child').outerWidth(true);
                    moveChild('last');
                    data.list.css('left', -offset);
                    data.list.animate({'left': 0}, data.settings.speed, function() {
                        data.enable = true;
                        handler.data('roll', data);
                    });
                    break;
                }
                case 'top': {
                    offset = data.list.find('li:first-child').outerHeight(true);
                    data.list.animate({'top': -offset}, data.settings.speed, function() {
                        moveChild('first');
                        data.list.css('top', 0);
                        data.enable = true;
                        handler.data('roll', data);
                    });
                    break;
                }
                case 'bottom': {
                    offset = data.list.find('li:last-child').outerHeight(true);
                    moveChild('last');
                    data.list.css('top', -offset);
                    data.list.animate({'top': 0}, data.settings.speed, function() {
                        data.enable = true;
                        handler.data('roll', data);
                    });
                    break;
                }
            }
			
			return handler;
        }
    };

    $.fn.roll = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }
        else if(typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        }
        else {
            $.error('Метод "' + method + '" не найден в плагине jQuery.roll');
        }
    };
})(jQuery);